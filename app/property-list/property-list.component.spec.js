
'use strict';

describe('component: propertyList', function() {
    var $componentController;
    var $httpBackend;
    var jsonResponse = {'results':[{id:'1'},{'id':'2'},{'id':'3'}],'saved':[{'id':'4'}]};

    beforeEach(module('reaApp'));
    beforeEach(inject(function (_$componentController_, _$httpBackend_) {
        $componentController = _$componentController_;
        $httpBackend =_$httpBackend_;
        $httpBackend.expectGET('data/data.json')
            .respond(jsonResponse);
    }));

    it('should retrieve a list of `results` objects from the json file', function () {
        var propList = $componentController('propertyList',null, jsonResponse);
        expect(propList.results).toBeDefined();
        expect(propList.results[0].id).toBe('1');
    });

    it('should retrieve a list of `saved` objects from the json file', function () {
        var propList = $componentController('propertyList',null, jsonResponse);

        expect(propList.saved).toBeDefined();
        expect(propList.saved[0].id).toBe('4');
    });

    it('should add selected item to the saved list', function() {
        var property = {id:'3'};
        var propList = $componentController('propertyList', null, jsonResponse, property);

        //check that property does not exist in the saved list before saving
        expect(propList.saved).not.toContain(property);
        propList.saveItem(property);
        //check that property exists in the saved list before saving
        expect(propList.saved).toContain(property);
    });

    it('should remove selected item from the saved list', function() {
        var property = {id:'3'};
        var propList = $componentController('propertyList', null, jsonResponse, property);

        //check that saved property exists in the saved list before removal
        expect(propList.saved).toContain(property);
        propList.removeSavedItem(property);
        //check that saved property does not exist in the saved list before removal
        expect(propList.saved).not.toContain(property);
    });

});

