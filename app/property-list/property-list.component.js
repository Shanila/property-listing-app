
angular.
module('propertyList').
component('propertyList', {
    templateUrl: 'property-list/property-list.template.html',
    controllerAs: 'list',
    controller: ['$http',
        function PropertyListController($http) {
            var list = this;

            $http.get('data/data.json').then(function (response) {
                list.properties = response.data.results;
                list.saved = response.data.saved;

            });

            this.saveItem = function(property) {
                if (list.saved.indexOf(property) == -1) {
                    list.saved.push(property);
                }
            }

            this.removeSavedItem = function(property){
                var indexVal = list.saved.indexOf(property);
                list.saved.splice(indexVal,1);
            }
        }
    ]
});