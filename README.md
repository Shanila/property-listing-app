# REA Property Listing app

A simple app listing property results and saved results. Properties listed in the Results column can be saved to the Saved properties column. Properties in the Saved Column can be removed.

### Node.js and Tools

Get [Node.js](https://nodejs.org/) version 6.7.0 preferably. Here's a great guide to installing a specific version (http://theholmesoffice.com/node-js-fundamentals-how-to-upgrade-the-node-js-version/)  

### Installing Dependencies

The application relies upon various Node.js tools, such as [Bower](https://bower.io/) and [Karma](https://karma-runner.github.io/). These can be installed these by running:

```
npm install
```

This will also run Bower, which will download the Angular files needed.

### Running the Application

- Run `npm start`.
- Navigate your browser to [http://localhost:8000/](http://localhost:8000/) to see the application 
  running.

### Unit Testing

- Start Karma with `npm test`.
- A browser will start and connect to the Karma server. Chrome and Firefox are the default browsers,
  others can be captured by loading the same URL or by changing the `karma.conf.js` file.
- Karma will watch the application and test JavaScript files.

## Application Directory Layout

```
app/                     --> all the source code of the app (including unit tests)
  bower_components/...   --> 3rd party JS/CSS libraries, including Angular and jQuery
  data/...               --> json files holding data
  property-list/...      --> files for the `propertyList` module, including JS source code, HTML templates, specs
  app.module.js          --> the main app module
  style.css              --> default stylesheet
  index.html             --> app layout file (the main HTML template file of the app)

node_modules/...         --> development tools (fetched using `npm`)

bower.json               --> Bower specific metadata, including client-side dependencies
karma.conf.js            --> config file for running unit tests with Karma
package.json             --> Node.js specific metadata, including development tools dependencies
```
